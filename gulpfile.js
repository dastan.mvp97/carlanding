const {series} = require('gulp'),
      gulp   = require('gulp'),
      sass   = require('gulp-sass'),
      browserSync = require('browser-sync').create();

function toCompileCss(){
    return gulp.src('./style/sass/*.scss')
           .pipe(sass())
           .pipe(gulp.dest('./style/css/'))
           .pipe(browserSync.stream());
}

function toRefresh(){
    browserSync.init({
        server:{
            baseDir:'./'
        }
    });
    gulp.watch('./style/sass/*.scss',toCompileCss);
    gulp.watch('./*.html').on('change',browserSync.reload);
}

exports.default = series(toCompileCss,toRefresh);