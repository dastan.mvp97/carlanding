// Function for jumbotron and carPicture
$(window).scroll(function(){
    // For two cars in jumbotron
    var wScroll = $(this).scrollTop();
    $('.car').css({
        'transform' : 'translate(0px, ' + wScroll/5 +'%)'
    });
    
    $('.blueCar').css({
        'transform' : 'translate(0px, ' + wScroll/7 +'%)'
    });
    
    $('.text').css({
        'transform' : 'translate(0px, '+ (-wScroll) +'%)',
        'opacity' : 1 - wScroll/500
    });
    // Function for carPicture in about section 
    if(wScroll > $('.carPictureBox').offset().top - $(this.window).height() / 2){
        $('.picture').addClass('is-showing-picture');
        $('.description').addClass('is-showing-description');
        if(wScroll > $('.scrollLine').offset().top - $(this.window).height() / 3){
            $('.Gallery img').each(function(i){
                setTimeout(function(){
                    $('.Gallery .imgClass').eq(i).addClass('is-showing-gallItem'); 
                },150*(i+1));
            });
        }
    }
    
});
