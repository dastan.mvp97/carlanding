// Function for jumbotron and carPicture
$(window).scroll(function(){
    // For two cars in jumbotron
    var wScroll = $(this).scrollTop();
    $('.car').css({
        'transform' : 'translate(0px, ' + wScroll/5 +'%)'
    });
    
    $('.blueCar').css({
        'transform' : 'translate(0px, ' + wScroll/7 +'%)'
    });
    
    $('.text').css({
        'transform' : 'translate(0px, '+ (-wScroll) +'%)',
        'opacity' : 1 - wScroll/500
    });
    // Function for carPicture in about section 
    if(wScroll > $('.content').offset().top - $(this.window).height() / 8){
        $('.carPictureBox').addClass('is-showing-car');
        $('.interiorPictureBox').addClass('is-showing-interior');
        if(wScroll > $('.colors').offset().top - $(this.window).height() / 2){
            $('.red').addClass('is-scaled-red');
            $('.blue').addClass('is-scaled-blue');
            $('.black').addClass('is-scaled-black');
            $('.white').addClass('is-scaled-white');
        }
    }
    
});

$('.red').click(function(){
    $('.redAston').addClass('is-showed-red');
    $('.blueAston').removeClass('is-showed-blue');
    $('.blackAston').removeClass('is-showed-black');
    $('.whiteAston').removeClass('is-showed-white');
});
$('.blue').click(function(){
    $('.blueAston').addClass('is-showed-blue');
    $('.redAston').removeClass('is-showed-red');
    $('.blackAston').removeClass('is-showed-black');
    $('.whiteAston').removeClass('is-showed-white');
});
$('.black').click(function(){
    $('.blackAston').addClass('is-showed-black');
    $('.blueAston').removeClass('is-showed-blue');
    $('.redAston').removeClass('is-showed-red');
    $('.whiteAston').removeClass('is-showed-white');
});
$('.white').click(function(){
    $('.whiteAston').addClass('is-showed-white');
    $('.blackAston').removeClass('is-showed-black');
    $('.blueAston').removeClass('is-showed-blue');
    $('.redAston').removeClass('is-showed-red');
});
